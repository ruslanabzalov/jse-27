package tsc.abzalov.tm.repository;

import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.IRepository;
import tsc.abzalov.tm.model.AbstractEntity;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractRepository<T extends AbstractEntity> implements IRepository<T> {

    @NotNull
    private final List<T> entities = new ArrayList<>();

    @Override
    public long size() {
        return entities.size();
    }

    @Override
    public boolean isEmpty() {
        return entities.isEmpty();
    }

    @Override
    public void create(@NotNull final T entity) {
        entities.add(entity);
    }

    @Override
    public void addAll(@NotNull List<T> entities) {
        this.entities.clear();
        this.entities.addAll(entities);
    }

    @Override
    @NotNull
    public List<T> findAll() {
        return entities;
    }

    @Override
    @Nullable
    public T findById(@NotNull final String id) {
        return entities.stream()
                .filter(entity -> {
                    @NotNull val currentId = entity.getId();
                    return id.equals(currentId);
                })
                .findFirst()
                .orElse(null);
    }

    @Override
    public void clear() {
        entities.clear();
    }

    @Override
    public void removeById(@NotNull final String id) {
        entities.removeIf(entity -> {
            @NotNull val currentId = entity.getId();
            return id.equals(currentId);
        });
    }

}
