package tsc.abzalov.tm.repository;

import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.repository.IUserRepository;
import tsc.abzalov.tm.model.User;

import java.util.List;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @NotNull
    private final List<User> users = findAll();

    @Override
    @Nullable
    public User findByLogin(@NotNull final String login) {
        return users.stream()
                .filter(user -> {
                    @Nullable val currentUserLogin = user.getLogin();
                    return login.equals(currentUserLogin);
                })
                .findFirst()
                .orElse(null);
    }

    @Override
    @Nullable
    public User findByEmail(@NotNull final String email) {
        return users.stream()
                .filter(user -> {
                    @Nullable val currentUserEmail = user.getEmail();
                    return email.equals(currentUserEmail);
                })
                .findFirst()
                .orElse(null);
    }

    @Override
    @Nullable
    public User editPassword(@NotNull final String id, @NotNull final String hashedPassword) {
        return users.stream()
                .filter(user -> {
                    @NotNull val currentUserId = user.getId();
                    return id.equals(currentUserId);
                })
                .findFirst()
                .map(user -> {
                    user.setHashedPassword(hashedPassword);
                    return user;
                })
                .orElse(null);
    }

    @Override
    @Nullable
    public User editUserInfo(@NotNull final String id, @NotNull final String firstName,
                             @Nullable final String lastName) {
        return users.stream()
                .filter(user -> {
                    @NotNull val currentUserId = user.getId();
                    return id.equals(currentUserId);
                })
                .findFirst()
                .map(user -> {
                    user.setFirstName(firstName);
                    user.setLastName(lastName);
                    return user;
                })
                .orElse(null);
    }

    @Override
    public void deleteByLogin(@NotNull final String login) {
        users.removeIf(user -> {
            @Nullable val currentUserLogin = user.getLogin();
            return login.equals(currentUserLogin);
        });
    }

    @Override
    @Nullable
    public User lockUnlockById(@NotNull final String id) {
        return users.stream()
                .filter(user -> {
                    @NotNull val currentUserId = user.getId();
                    return id.equals(currentUserId);
                })
                .findFirst()
                .map(user -> {
                    user.setLocked(!user.isLocked());
                    return user;
                })
                .orElse(null);
    }

    @Override
    @Nullable
    public User lockUnlockByLogin(@NotNull final String login) {
        return users.stream()
                .filter(user -> {
                    @Nullable val currentUserLogin = user.getLogin();
                    return login.equals(currentUserLogin);
                })
                .findFirst()
                .map(user -> {
                    user.setLocked(!user.isLocked());
                    return user;
                })
                .orElse(null);
    }

}
