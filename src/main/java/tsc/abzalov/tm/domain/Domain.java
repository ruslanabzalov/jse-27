package tsc.abzalov.tm.domain;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.model.Project;
import tsc.abzalov.tm.model.Task;
import tsc.abzalov.tm.model.User;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
public final class Domain implements Serializable {

    @Nullable
    private List<Project> projects;

    @Nullable
    private List<Task> tasks;

    @Nullable
    private List<User> users;

}
