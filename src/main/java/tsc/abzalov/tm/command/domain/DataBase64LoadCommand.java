package tsc.abzalov.tm.command.domain;

import lombok.SneakyThrows;
import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.IServiceLocator;
import tsc.abzalov.tm.domain.Domain;
import tsc.abzalov.tm.enumeration.CommandType;

import java.io.ByteArrayInputStream;
import java.io.ObjectInputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

import static tsc.abzalov.tm.enumeration.CommandType.ADMIN_COMMAND;
import static tsc.abzalov.tm.util.SystemUtil.BASE64_DECODER;

@SuppressWarnings("unused")
public final class DataBase64LoadCommand extends AbstractDomainCommand {

    public DataBase64LoadCommand(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @NotNull
    public String getCommandName() {
        return "data-base64-load";
    }

    @Override
    @Nullable
    public String getCommandArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Load data from Base64 format.";
    }

    @Override
    @NotNull
    public CommandType getCommandType() {
        return ADMIN_COMMAND;
    }

    @Override
    @SneakyThrows
    public void execute() {
        @NotNull val base64Data = new String(Files.readAllBytes(Paths.get(BASE64_FILENAME)));
        val decodedBase64Data = BASE64_DECODER.decode(base64Data);

        @NotNull val byteArrayInputStream = new ByteArrayInputStream(decodedBase64Data);
        @NotNull val objectInputStream = new ObjectInputStream(byteArrayInputStream);
        @NotNull val domain = (Domain) objectInputStream.readObject();
        setDomain(domain);

        objectInputStream.close();
        byteArrayInputStream.close();

        System.out.println("Data was loaded from Base64 format.\nPlease, re-login.");
    }

}
