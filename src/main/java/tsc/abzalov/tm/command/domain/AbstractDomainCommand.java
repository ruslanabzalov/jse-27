package tsc.abzalov.tm.command.domain;

import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.IServiceLocator;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.domain.Domain;

public abstract class AbstractDomainCommand extends AbstractCommand {

    @NotNull
    public static final String BINARY_FILENAME = "data.bin";

    @NotNull
    public static final String BASE64_FILENAME = "data.base64";

    public AbstractDomainCommand(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    public Domain getDomain() {
        @NotNull val serviceLocator = getServiceLocator();
        @NotNull val projectService = serviceLocator.getProjectService();
        @NotNull val taskService = serviceLocator.getTaskService();
        @NotNull val userService = serviceLocator.getUserService();
        @NotNull val projects = projectService.findAll();
        @NotNull val tasks = taskService.findAll();
        @NotNull val users = userService.findAll();

        @NotNull val domain = new Domain();
        domain.setProjects(projects);
        domain.setTasks(tasks);
        domain.setUsers(users);
        return domain;
    }

    public void setDomain(@Nullable Domain domain) {
        if (domain == null) return;

        @NotNull val serviceLocator = getServiceLocator();
        @NotNull val projectService = serviceLocator.getProjectService();
        @NotNull val taskService = serviceLocator.getTaskService();
        @NotNull val userService = serviceLocator.getUserService();
        @NotNull val authService = serviceLocator.getAuthService();

        projectService.clear();
        projectService.addAll(domain.getProjects());

        taskService.clear();
        taskService.addAll(domain.getTasks());

        userService.clear();
        userService.addAll(domain.getUsers());

        authService.logoff();
    }

}
