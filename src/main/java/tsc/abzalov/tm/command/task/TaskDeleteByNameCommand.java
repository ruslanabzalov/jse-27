package tsc.abzalov.tm.command.task;

import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.IServiceLocator;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.enumeration.CommandType;

import static tsc.abzalov.tm.enumeration.CommandType.TASK_COMMAND;
import static tsc.abzalov.tm.util.InputUtil.inputName;

@SuppressWarnings("unused")
public final class TaskDeleteByNameCommand extends AbstractCommand {

    public TaskDeleteByNameCommand(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @NotNull
    public String getCommandName() {
        return "delete-task-by-name";
    }

    @Override
    @Nullable
    public String getCommandArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Delete task by name.";
    }

    @Override
    @NotNull
    public CommandType getCommandType() {
        return TASK_COMMAND;
    }

    @Override
    public void execute() {
        System.out.println("DELETE TASK BY NAME\n");
        @NotNull val serviceLocator = getServiceLocator();
        @NotNull val taskService = serviceLocator.getTaskService();
        @NotNull val authService = serviceLocator.getAuthService();
        @NotNull val currentUserId = authService.getCurrentUserId();

        val areTasksExist = taskService.size(currentUserId) != 0;
        if (areTasksExist) {
            taskService.removeByName(currentUserId, inputName());
            System.out.println("Task was successfully deleted.\n");
            return;
        }

        System.out.println("Tasks list is empty.\n");
    }

}
