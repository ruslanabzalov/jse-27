package tsc.abzalov.tm.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.Serializable;
import java.util.Optional;

import static tsc.abzalov.tm.util.LiteralConst.DEFAULT_REFERENCE;

@Data
@EqualsAndHashCode(callSuper = true)
public final class Task extends BusinessEntity implements Serializable {

    @Nullable
    private String projectId;

    @Override
    @NotNull
    public String toString() {
        @NotNull val correctProjectId = Optional.ofNullable(projectId).orElse(DEFAULT_REFERENCE);
        @NotNull val superStringInterpretation = super.toString();

        return superStringInterpretation.replace("]", "; Project ID: " + correctProjectId + "]");
    }

}
